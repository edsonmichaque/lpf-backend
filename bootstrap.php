<?php
  require 'vendor/autoload.php';

  date_default_timezone_set('africa/maputo');
  
  use Symfony\Component\Yaml\Yaml;
  use Doctrine\ORM\Tools\Setup;
  use Doctrine\ORM\EntityManager;
  use Doctrine\ORM\Configuration;
  use JsonSchema\Uri\UriRetriever;
  use JsonSchema\Validator;
  use Slim\Slim;

  $dev = true;

  $db = null;

  if (getenv('HEROKU')) {
    $db = Yaml::parse(__DIR__ . '/config/db.yaml.heroku');
  } else {
    $db = Yaml::parse(__DIR__ . '/config/db.yaml');
  }

  $dbParams = array(
    'driver' => $db['adapter'],
    'user' => $db['user'],
    'password' => $db['password'],
    'dbname' => $db['database'],
    'port' => $db['port'],
    'host' => $db['host']
  );

  $pdo = new PDO(
    $db['pdo_adapter'] . ':host=' . $db['host'] . ';dbname=' . $db['database'] . ';port=' . $db['port'], 
    $db['user'], 
    $db['password']
  );

  $dbConfig = Setup::createYAMLMetadataConfiguration(array(__DIR__."/app/config/yaml"), $dev);
  $entityManager = EntityManager::create($dbParams, $dbConfig);
  $em = $entityManager;
  $app = new Slim(array('debug' => $dev));
  $retriever = new UriRetriever;
  $schemaDir = __DIR__ . '/app/schemas/';
  #$s = $retriever->retriever($schemaDir = __DIR__ . '/app/schemas/');
  $schemaValidator = new Validator;
?>
