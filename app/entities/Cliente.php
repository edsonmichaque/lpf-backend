<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente")
 * @ORM\Entity
 */
class Cliente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cliente_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string")
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string")
     */
    private $descricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_criacao", type="datetime")
     */
    private $dataDeCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_actualizacao", type="datetime")
     */
    private $dataDeActualizacao;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Localizacao", inversedBy="clientes")
     * @ORM\JoinTable(name="cliente_localizacao",
     *   joinColumns={
     *     @ORM\JoinColumn(name="cliente", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="localizacao", referencedColumnName="id")
     *   }
     * )
     */
    private $localizacoes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->localizacoes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Cliente
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Cliente
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set dataDeCriacao
     *
     * @param \DateTime $dataDeCriacao
     * @return Cliente
     */
    public function setDataDeCriacao($dataDeCriacao)
    {
        $this->dataDeCriacao = $dataDeCriacao;

        return $this;
    }

    /**
     * Get dataDeCriacao
     *
     * @return \DateTime 
     */
    public function getDataDeCriacao()
    {
        return $this->dataDeCriacao;
    }

    /**
     * Set dataDeActualizacao
     *
     * @param \DateTime $dataDeActualizacao
     * @return Cliente
     */
    public function setDataDeActualizacao($dataDeActualizacao)
    {
        $this->dataDeActualizacao = $dataDeActualizacao;

        return $this;
    }

    /**
     * Get dataDeActualizacao
     *
     * @return \DateTime 
     */
    public function getDataDeActualizacao()
    {
        return $this->dataDeActualizacao;
    }

    /**
     * Add localizacoes
     *
     * @param \Localizacao $localizacoes
     * @return Cliente
     */
    public function addLocalizaco(\Localizacao $localizacoes)
    {
        $this->localizacoes[] = $localizacoes;

        return $this;
    }

    /**
     * Remove localizacoes
     *
     * @param \Localizacao $localizacoes
     */
    public function removeLocalizaco(\Localizacao $localizacoes)
    {
        $this->localizacoes->removeElement($localizacoes);
    }

    /**
     * Get localizacoes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocalizacoes()
    {
        return $this->localizacoes;
    }
}
