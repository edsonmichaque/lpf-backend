<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Farmacia
 *
 * @ORM\Table(name="farmacias")
 * @ORM\Entity
 */
class Farmacia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="farmacias_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", nullable=true)
     */
    private $nome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_criacao", type="datetime", nullable=true)
     */
    private $dataDeCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_actualizacao", type="datetime", nullable=true)
     */
    private $dataDeActualizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", length=160, nullable=true)
     */
    private $descricao;

    /**
     * @var \Localizacao
     *
     * @ORM\OneToOne(targetEntity="Localizacao", inversedBy="farmacia", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="localizacao", referencedColumnName="id", unique=true)
     * })
     */
    private $localizacao;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Stock", mappedBy="farmacia", cascade={"all"})
     */
    private $stockes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stockes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Farmacia
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set dataDeCriacao
     *
     * @param \DateTime $dataDeCriacao
     * @return Farmacia
     */
    public function setDataDeCriacao($dataDeCriacao)
    {
        $this->dataDeCriacao = $dataDeCriacao;

        return $this;
    }

    /**
     * Get dataDeCriacao
     *
     * @return \DateTime 
     */
    public function getDataDeCriacao()
    {
        return $this->dataDeCriacao;
    }

    /**
     * Set dataDeActualizacao
     *
     * @param \DateTime $dataDeActualizacao
     * @return Farmacia
     */
    public function setDataDeActualizacao($dataDeActualizacao)
    {
        $this->dataDeActualizacao = $dataDeActualizacao;

        return $this;
    }

    /**
     * Get dataDeActualizacao
     *
     * @return \DateTime 
     */
    public function getDataDeActualizacao()
    {
        return $this->dataDeActualizacao;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Farmacia
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set localizacao
     *
     * @param \Localizacao $localizacao
     * @return Farmacia
     */
    public function setLocalizacao(\Localizacao $localizacao = null)
    {
        $this->localizacao = $localizacao;

        return $this;
    }

    /**
     * Get localizacao
     *
     * @return \Localizacao 
     */
    public function getLocalizacao()
    {
        return $this->localizacao;
    }

    /**
     * Add stockes
     *
     * @param \Stock $stockes
     * @return Farmacia
     */
    public function addStocke(\Stock $stockes)
    {
        $stockes->setFarmacia($this);
        $this->stockes[] = $stockes;

        return $this;
    }

    /**
     * Remove stockes
     *
     * @param \Stock $stockes
     */
    public function removeStocke(\Stock $stockes)
    {
        $this->stockes->removeElement($stockes);
    }

    /**
     * Get stockes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStockes()
    {
        return $this->stockes;
    }
}
