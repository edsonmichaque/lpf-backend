<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Stock
 *
 * @ORM\Table(name="stockes")
 * @ORM\Entity
 */
class Stock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="stockes_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=128, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=128, nullable=true)
     */
    private $descricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_criacao", type="datetime", nullable=true)
     */
    private $dataDeCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_actualizacao", type="datetime", nullable=true)
     */
    private $dataDeActualizacao;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Categoria", mappedBy="stock")
     */
    private $categorias;

    /**
     * @var \Farmacia
     *
     * @ORM\ManyToOne(targetEntity="Farmacia", inversedBy="stockes", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="farmacia", referencedColumnName="id")
     * })
     */
    private $farmacia;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categorias = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Stock
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Stock
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set dataDeCriacao
     *
     * @param \DateTime $dataDeCriacao
     * @return Stock
     */
    public function setDataDeCriacao($dataDeCriacao)
    {
        $this->dataDeCriacao = $dataDeCriacao;

        return $this;
    }

    /**
     * Get dataDeCriacao
     *
     * @return \DateTime 
     */
    public function getDataDeCriacao()
    {
        return $this->dataDeCriacao;
    }

    /**
     * Set dataDeActualizacao
     *
     * @param \DateTime $dataDeActualizacao
     * @return Stock
     */
    public function setDataDeActualizacao($dataDeActualizacao)
    {
        $this->dataDeActualizacao = $dataDeActualizacao;

        return $this;
    }

    /**
     * Get dataDeActualizacao
     *
     * @return \DateTime 
     */
    public function getDataDeActualizacao()
    {
        return $this->dataDeActualizacao;
    }

    /**
     * Add categorias
     *
     * @param \Categoria $categorias
     * @return Stock
     */
    public function addCategoria(\Categoria $categorias)
    {
        $categorias->setStock($this);
        $this->categorias[] = $categorias;

        return $this;
    }

    /**
     * Remove categorias
     *
     * @param \Categoria $categorias
     */
    public function removeCategoria(\Categoria $categorias)
    {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Set farmacia
     *
     * @param \Farmacia $farmacia
     * @return Stock
     */
    public function setFarmacia(\Farmacia $farmacia = null)
    {
        $this->farmacia = $farmacia;

        return $this;
    }

    /**
     * Get farmacia
     *
     * @return \Farmacia 
     */
    public function getFarmacia()
    {
        return $this->farmacia;
    }
}
