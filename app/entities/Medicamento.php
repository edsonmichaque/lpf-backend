<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Medicamento
 *
 * @ORM\Table(name="medicamentos")
 * @ORM\Entity
 */
class Medicamento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="medicamentos_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string")
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string")
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="preco", type="decimal")
     */
    private $preco;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer")
     */
    private $quantidade;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_criacao", type="datetime")
     */
    private $dataDeDriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_actualizacao", type="datetime")
     */
    private $dataDeActualizacao;

    /**
     * @var \Categoria
     *
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="medicamentos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria", referencedColumnName="id")
     * })
     */
    private $categoria;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Marcador", inversedBy="medicamentos")
     * @ORM\JoinTable(name="marcadores_medicamentos",
     *   joinColumns={
     *     @ORM\JoinColumn(name="medicamento", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="marcador", referencedColumnName="id")
     *   }
     * )
     */
    private $marcadores;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->marcadores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Medicamento
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Medicamento
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set preco
     *
     * @param string $preco
     * @return Medicamento
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;

        return $this;
    }

    /**
     * Get preco
     *
     * @return string 
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * Set quantidade
     *
     * @param integer $quantidade
     * @return Medicamento
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Get quantidade
     *
     * @return integer 
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * Set dataDeDriacao
     *
     * @param \DateTime $dataDeDriacao
     * @return Medicamento
     */
    public function setDataDeDriacao($dataDeDriacao)
    {
        $this->dataDeDriacao = $dataDeDriacao;

        return $this;
    }

    /**
     * Get dataDeDriacao
     *
     * @return \DateTime 
     */
    public function getDataDeDriacao()
    {
        return $this->dataDeDriacao;
    }

    /**
     * Set dataDeActualizacao
     *
     * @param \DateTime $dataDeActualizacao
     * @return Medicamento
     */
    public function setDataDeActualizacao($dataDeActualizacao)
    {
        $this->dataDeActualizacao = $dataDeActualizacao;

        return $this;
    }

    /**
     * Get dataDeActualizacao
     *
     * @return \DateTime 
     */
    public function getDataDeActualizacao()
    {
        return $this->dataDeActualizacao;
    }

    /**
     * Set categoria
     *
     * @param \Categoria $categoria
     * @return Medicamento
     */
    public function setCategoria(\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \Categoria 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Add marcadores
     *
     * @param \Marcador $marcadores
     * @return Medicamento
     */
    public function addMarcadore(\Marcador $marcadores)
    {
        $this->marcadores[] = $marcadores;

        return $this;
    }

    /**
     * Remove marcadores
     *
     * @param \Marcador $marcadores
     */
    public function removeMarcadore(\Marcador $marcadores)
    {
        $this->marcadores->removeElement($marcadores);
    }

    /**
     * Get marcadores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMarcadores()
    {
        return $this->marcadores;
    }
    /**
     * @var \DateTime
     */
    private $dataDeCriacao;


    /**
     * Set dataDeCriacao
     *
     * @param \DateTime $dataDeCriacao
     * @return Medicamento
     */
    public function setDataDeCriacao($dataDeCriacao)
    {
        $this->dataDeCriacao = $dataDeCriacao;

        return $this;
    }

    /**
     * Get dataDeCriacao
     *
     * @return \DateTime 
     */
    public function getDataDeCriacao()
    {
        return $this->dataDeCriacao;
    }
}
