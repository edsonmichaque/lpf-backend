<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Categoria
 *
 * @ORM\Table(name="categoria")
 * @ORM\Entity
 */
class Categoria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="categoria_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string")
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string")
     */
    private $descricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_criacao", type="datetime")
     */
    private $dataDeCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_actualizacao", type="datetime")
     */
    private $dataDeActualizacao;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Medicamento", mappedBy="categoria")
     */
    private $medicamentos;

    /**
     * @var \Stock
     *
     * @ORM\ManyToOne(targetEntity="Stock", inversedBy="categorias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock", referencedColumnName="id")
     * })
     */
    private $stock;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medicamentos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Categoria
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Categoria
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set dataDeCriacao
     *
     * @param \DateTime $dataDeCriacao
     * @return Categoria
     */
    public function setDataDeCriacao($dataDeCriacao)
    {
        $this->dataDeCriacao = $dataDeCriacao;

        return $this;
    }

    /**
     * Get dataDeCriacao
     *
     * @return \DateTime 
     */
    public function getDataDeCriacao()
    {
        return $this->dataDeCriacao;
    }

    /**
     * Set dataDeActualizacao
     *
     * @param \DateTime $dataDeActualizacao
     * @return Categoria
     */
    public function setDataDeActualizacao($dataDeActualizacao)
    {
        $this->dataDeActualizacao = $dataDeActualizacao;

        return $this;
    }

    /**
     * Get dataDeActualizacao
     *
     * @return \DateTime 
     */
    public function getDataDeActualizacao()
    {
        return $this->dataDeActualizacao;
    }

    /**
     * Add medicamentos
     *
     * @param \Medicamento $medicamentos
     * @return Categoria
     */
    public function addMedicamento(\Medicamento $medicamentos)
    {
        $medicamentos->setCategoria($this);
        $this->medicamentos[] = $medicamentos;

        return $this;
    }

    /**
     * Remove medicamentos
     *
     * @param \Medicamento $medicamentos
     */
    public function removeMedicamento(\Medicamento $medicamentos)
    {
        $medicamentos->setCategoria(null);
        $this->medicamentos->removeElement($medicamentos);
    }

    /**
     * Get medicamentos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedicamentos()
    {
        return $this->medicamentos;
    }

    /**
     * Set stock
     *
     * @param \Stock $stock
     * @return Categoria
     */
    public function setStock(\Stock $stock = null)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \Stock 
     */
    public function getStock()
    {
        return $this->stock;
    }
}
