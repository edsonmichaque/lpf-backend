<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Localizacao
 *
 * @ORM\Table(name="localizacoes")
 * @ORM\Entity
 */
class Localizacao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="localizacoes_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal")
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal")
     */
    private $longitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_criacao", type="datetime")
     */
    private $dataDeDriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_actualizacao", type="datetime")
     */
    private $dataDeActualizacao;

    /**
     * @var \Farmacia
     *
     * @ORM\OneToOne(targetEntity="Farmacia", mappedBy="localizacao")
     */
    private $farmacia;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Cliente", mappedBy="localizacoes")
     */
    private $clientes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Localizacao
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return Localizacao
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set dataDeDriacao
     *
     * @param \DateTime $dataDeDriacao
     * @return Localizacao
     */
    public function setDataDeDriacao($dataDeDriacao)
    {
        $this->dataDeDriacao = $dataDeDriacao;

        return $this;
    }

    /**
     * Get dataDeDriacao
     *
     * @return \DateTime 
     */
    public function getDataDeDriacao()
    {
        return $this->dataDeDriacao;
    }

    /**
     * Set dataDeActualizacao
     *
     * @param \DateTime $dataDeActualizacao
     * @return Localizacao
     */
    public function setDataDeActualizacao($dataDeActualizacao)
    {
        $this->dataDeActualizacao = $dataDeActualizacao;

        return $this;
    }

    /**
     * Get dataDeActualizacao
     *
     * @return \DateTime 
     */
    public function getDataDeActualizacao()
    {
        return $this->dataDeActualizacao;
    }

    /**
     * Set farmacia
     *
     * @param \Farmacia $farmacia
     * @return Localizacao
     */
    public function setFarmacia(\Farmacia $farmacia = null)
    {
        $this->farmacia = $farmacia;

        return $this;
    }

    /**
     * Get farmacia
     *
     * @return \Farmacia 
     */
    public function getFarmacia()
    {
        return $this->farmacia;
    }

    /**
     * Add clientes
     *
     * @param \Cliente $clientes
     * @return Localizacao
     */
    public function addCliente(\Cliente $clientes)
    {
        $this->clientes[] = $clientes;

        return $this;
    }

    /**
     * Remove clientes
     *
     * @param \Cliente $clientes
     */
    public function removeCliente(\Cliente $clientes)
    {
        $this->clientes->removeElement($clientes);
    }

    /**
     * Get clientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientes()
    {
        return $this->clientes;
    }
}
