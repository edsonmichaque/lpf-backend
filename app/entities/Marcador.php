<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Marcador
 *
 * @ORM\Table(name="marcadores")
 * @ORM\Entity
 */
class Marcador
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="marcadores_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="palavra", type="string", length=128)
     */
    private $palavra;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=128)
     */
    private $descricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_criacao", type="datetime")
     */
    private $dataDeDriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_de_actualizacao", type="datetime")
     */
    private $dataDeActualizacao;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Medicamento", mappedBy="marcadores")
     */
    private $medicamentos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medicamentos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set palavra
     *
     * @param string $palavra
     * @return Marcador
     */
    public function setPalavra($palavra)
    {
        $this->palavra = $palavra;

        return $this;
    }

    /**
     * Get palavra
     *
     * @return string 
     */
    public function getPalavra()
    {
        return $this->palavra;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Marcador
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set dataDeDriacao
     *
     * @param \DateTime $dataDeDriacao
     * @return Marcador
     */
    public function setDataDeDriacao($dataDeDriacao)
    {
        $this->dataDeDriacao = $dataDeDriacao;

        return $this;
    }

    /**
     * Get dataDeDriacao
     *
     * @return \DateTime 
     */
    public function getDataDeDriacao()
    {
        return $this->dataDeDriacao;
    }

    /**
     * Set dataDeActualizacao
     *
     * @param \DateTime $dataDeActualizacao
     * @return Marcador
     */
    public function setDataDeActualizacao($dataDeActualizacao)
    {
        $this->dataDeActualizacao = $dataDeActualizacao;

        return $this;
    }

    /**
     * Get dataDeActualizacao
     *
     * @return \DateTime 
     */
    public function getDataDeActualizacao()
    {
        return $this->dataDeActualizacao;
    }

    /**
     * Add medicamentos
     *
     * @param \Medicamento $medicamentos
     * @return Marcador
     */
    public function addMedicamento(\Medicamento $medicamentos)
    {   $medicamento->addMarcador($this);
        $this->medicamentos[] = $medicamentos;

        return $this;
    }

    /**
     * Remove medicamentos
     *
     * @param \Medicamento $medicamentos
     */
    public function removeMedicamento(\Medicamento $medicamentos)
    {
        $this->medicamentos->removeElement($medicamentos);
    }

    /**
     * Get medicamentos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedicamentos()
    {
        return $this->medicamentos;
    }
    /**
     * @var \DateTime
     */
    private $dataDeCriacao;


    /**
     * Set dataDeCriacao
     *
     * @param \DateTime $dataDeCriacao
     * @return Marcador
     */
    public function setDataDeCriacao($dataDeCriacao)
    {
        $this->dataDeCriacao = $dataDeCriacao;

        return $this;
    }

    /**
     * Get dataDeCriacao
     *
     * @return \DateTime 
     */
    public function getDataDeCriacao()
    {
        return $this->dataDeCriacao;
    }
}
