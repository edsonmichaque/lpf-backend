<?php

require __DIR__ . '/../bootstrap.php';

$validator = array(
  'retriever' => $retriever,
  'dir' => __DIR__ . '/schemas/json',
  'validator' => $schemaValidator
);

$contentType = array(
  'json' => 'application/json',
  'jsonapi' => 'application/vnd.api+json; version 1'
);

$erros = array (
  'validacao' => array (
    'estado' => 'Erro',
    'codigo' => '400',
    'descricao' => 'Erro de validacao'
  ),

  'criacao' => array (
    'estado' => 'Erro',
    'codigo' => '400',
    'descricao' => 'Erro de criacao'
  ),

  'formato' => array (
    'estado' => 'Erro',
    'codigo' => '400',
    'descricao' => 'formato nao suportado'
  )    
);


$app->notFound(function() use ($app) {
  $r = array(
    'estado' => 'Erro',
    'codigo' => 404,
    'descricao' => 'Recurso nao encontrado'
  );

  $app->response->headers->set('Content-Type', 'application/json');
  $app->halt(404, json_encode($r));
});

$app->get('/', function() use ($app, $em, $validator, $contentType, $erros) {
  $response = array();

  $response["id"] = 1;
  $response["nome"] = "Farmacia Foo";
  $response["descricao"] = "blah blah blah";

  $response = array('farmacias' => $response);
  $app->response->headers->set('Content-Type', 'application/vnd.api+json');
  $app->response->headers->set('Control-Allow-Origin',  '*');
  $app->response->setStatus(200);
  $app->response->setBody(json_encode($response));

});



$app->group('/farmacias', function() use($app, $em, $validator, $contentType, $erros) {
  $app->get('/', function() use ($app, $em, $validator, $contentType, $erros) {
    $fs = null;
    $app->response->headers->set('Content-Type', $contentType['json']);
    $offset = $app->request->params('offset');
    $limit = $app->request->params('limit');

    $existeFarmacias = true;
    if ($offset !== null && $limit !== null) {
      $em->getConnection()->beginTransaction();
      try {
        $fs = $em->getRepository('Farmacia')->findBy(array(), array(), $limit, $offset);
      } catch (Exception $e) {
        $existeFarmacias = false;
      }
    } else if ($offset !== null && $limit === null) {
      $em->getConnection()->beginTransaction();
      try {
        $fs = $em->getRepository('Farmacia')->findBy(array(), array(), null, $offset);
      } catch (Exception $e) {
        $existeFarmacias = false;
      }
    } else if ($limit !== null && $offset === null) {
      $em->getConnection()->beginTransaction();
      try {
        $fs = $em->getRepository('Farmacia')->findBy(array(), array(), $limit, 0);
      } catch (Exception $e) {
        $existeFarmacias = false; 
      }
    } else {
      $em->getConnection()->beginTransaction();
      try {
        $fs = $em->getRepository('Farmacia')->findAll();
      } catch (Exception $e) {
        $existeFarmacias = false;
      }
    }

    $r = array();
    if ($fs !== null || !$existeFarmacias) {
      foreach ($fs as $f) {
        $farmacia = array(
          'id' => $f->getId(),
          'nome' => $f->getNome(),
          'descricao' => $f->getDescricao(),
          'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
          'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')); 
        $r[] = $farmacia;
      }

      $links = null;
      if($offset !== null && $limit !== null) {
        if (($offset - $limit) >= $limit) {
        $links = array(
          'self' => $app->urlFor('farmacias') . '?offset=' . $offset . '&limit=' . $limit,
          'proximo' => $app->urlFor('farmacias') . '?offset=' . ($offset + $limit) . '&limit=' . $limit,
          'anterior' => $app->urlFor('farmacias') . '?offset=' . ($offset - $limit) . '&limit=' . $limit);
        } else{
          $links = array(
          'self' => $app->urlFor('farmacias') . '?offset=' . $offset . '&limit=' . $limit,
          'proximo' => $app->urlFor('farmacias') . '?offset=' . ($offset + $limit) . '&limit=' . $limit);
        }
      }

      $app->response->setBody(json_encode(array('farmacias' => $r, 'links' => $links)));
      $app->response->setStatus(200);
    } else {
      $app->response->setBody(json_encode($erros['criacao']));
      $app->response->setStatus(404);
    }
  })->name('farmacias');

  $app->post('/', function() use ($app, $em, $validator, $contentType, $erros) {
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
    $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/post.json');
    $data = json_decode($app->request->getBody());
    
    $r = null;
    if ($app->request->headers->get('Content-Type') === $contentType['json'] || $app->request->headers->get('Content-Type') === $contentType['jsonapi']) {
      $validator['validator']->check($data, $schema);
      if ($validator['validator']->isValid()) {
        $farmacia = json_decode($app->request->getBody(), true);

        $nome = $farmacia['nome'];
        $descricao = $farmacia['descricao'];

        $em->getConnection()->beginTransaction();
        $farmaciaCriada = true;
        try {
          $f = new Farmacia;
          $f->setNome($nome);
          $f->setDescricao($descricao);
          $f->setDataDeActualizacao(new DateTime('NOW'));
          $f->setDataDeCriacao(new DateTime('NOW'));

          $em->persist($f);
          $em->flush();
          $em->getConnection()->commit();
        } catch(Exception $e) {
          $em->getConnection()->rollback();
          $farmaciaCriada = false;
        }

        if ($farmaciaCriada) {
          $r = array(
            'farmacias' => array(
              'id' => $f->getId(),
              'nome' => $f->getNome(),
              'descricao' => $f->getDescricao(),
              'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
              'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
            )
          );
          $app->response->setStatus(201);
        } else {
          $r = $erros['criacao'];
        }
      } else {
        $r = $erros['validacao'];
      }
    } else {
      $r = $erros['formato'];
      $app->response->setStatus(400);
    }

    $app->response->setBody(json_encode($r));
  })->name('post_farmacias');

  $app->put('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){
      $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
      $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/put.json');
      $data = json_decode($app->request->getBody());
      
      $r = null;
      if ($app->request->headers->get('Content-Type') === 'application/vnd.api+json; version 1') {
        $validator['validator']->check($data, $schema);
        if ($validator['validator']->isValid()) {
          $farmacia = json_decode($app->request->getBody(), true);

          $nome = $farmacia['nome'];
          $descricao = $farmacia['descricao'];
          
          $f = $em->find('Farmacia', $id);

          if ($f !== null) {
            $f->setNome($nome);
            $f->setDescricao($descricao);
            $f->setDataDeActualizacao(new DateTime('NOW'));

            $em->persist($f);
            $em->flush();

            $r = array(
              'farmacias' => array(
                'id' => $f->getId(),
                'nome' => $f->getNome(),
                'descricao' => $f->getDescricao(),
                'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
                'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
              )
            );
            $app->response->setStatus(200);
          } else {
            $r = $erros['criacao'];

            $app->response->setStatus(404);  
          }
          
        } else {
          $r = $erros['validacao'];

          $app->response->setStatus(400);

        }
      } else {
        $r = $erros['formato'];
        $app->response->setStatus(400);
      }

      $app->response->setBody(json_encode($r));

  })->name('put_farmacias');

  $app->get('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros) {
    $f = $em->find('Farmacia', $id);
    $r = null;
    if ($f !== null) {
       $r = array(
        'farmacias' => array(
          'id' => $f->getId(),
          'nome' => $f->getNome(),
          'descricao' => $f->getDescricao(),
          'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
          'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
        )
      );
      $app->response->setStatus(200);
    } else {
      $r = $erros['criacao']; # nao encontrado
      $app->response->setStatus(404);  
    }
    $app->response->setBody(json_encode($r));
  })->name('get_farmacia');

  $app->delete('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){
    $f = $em->find('Farmacia', $id);
    $r = null;
    if ($f !== null) {
      $em->remove($f);
      $em->flush();
      $r = array(
        'farmacias' => array(
          'id' => $f->getId(),
          'nome' => $f->getNome(),
          'descricao' => $f->getDescricao(),
          'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
          'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
        )
      );
      $app->response->setStatus(200);
    } else {
      $r = $erros['criacao']; #nao encontrado

      $app->response->setStatus(404);  
    }
    $app->response->setBody(json_encode($r));
  })->name('delete_farmacias');

  #Nao implementado ainda
  $app->patch('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){})->name('patch_farmacia');


  $app->get('/:id/localizacao', function($id) use ($app, $em, $validator, $contentType, $erros, $validator) {

  })->name('localizacao_da_farmacia');
  $app->post('/:id/localizacao', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->put('/:id/localizacao', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->delete('/:id/localizacao', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->patch('/:id/localizacao', function($id) use ($app, $em, $validator, $contentType, $erros){});

  $app->get('/:id/stockes/?', function($id) use ($app, $em, $validator, $contentType, $erros){})->name('stock_da_farmacia');;
  $app->get('/:id/stockes/:sid/?', function($id, $sid) use ($app, $em, $validator, $contentType, $erros) {
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');

    $r = null;

    $f = $em->find('Farmacia', $id);
    $s = $em->getRepository('Stock')->find($sid);
    
    if ($f !== null) {
      if ($s !== null) {
        if ($f->getStockes()->contains($s)) {
          $r = array(
            'farmacias' => array(
              'id' => $s->getId(),
              'nome' => $s->getNome(),
              'descricao' => $s->getDescricao(),
              'dataDeCriacao' => $s->getDataDeCriacao()->format('Y-m-d H:i:s'),
              'dataDeActualizacao' => $s->getDataDeActualizacao()->format('Y-m-d H:i:s')
            )
          );
                 
          $app->response->setStatus(400);
        } else {
          $r = $erros['criacao']; # na existe $s em $f
        }
      } else {
        $r = $erros['criacao']; # stock nao encontrado
      }
    } else {
      $r = $erros['criacao']; #farmacia nao encntrada
    } 
    
    $app->response->setBody(json_encode($r));
  })->name('stock_da_farmacia');;
  
  $app->post('/:id/stockes/?', function($id) use ($app, $em, $validator, $contentType, $erros) {
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
    $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/stockes/post.json');
    $data = json_decode($app->request->getBody());
    
    $r = null;
    if ($app->request->headers->get('Content-Type') === $contentType['json'] || $app->request->headers->get('Content-Type') === $contentType['jsonapi']) {
      $validator['validator']->check($data, $schema);
      if ($validator['validator']->isValid()) {
        $farmacia = json_decode($app->request->getBody(), true);

        $nome = $farmacia['nome'];
        $descricao = $farmacia['descricao'];

        $f = $em->find('Farmacia', $id);
        if ($f !== null) {
          $em->getConnection()->beginTransaction();
          
          $stockCriado = true;
          try {
            $s = new Stock;
            $s->setNome($nome);
            $s->setDescricao($descricao);
            $s->setDataDeCriacao(new DateTime('NOW'));
            $s->setDataDeActualizacao(new DateTime('NOW'));

            $f->addStocke($s);

            $em->persist($f);
            $em->flush();
            $em->getConnection()->commit();
          } catch (Exception $e) {
            $stockCriado = false;
            $em->getConnection()->rollback();
          }

          if ($stockCriado) {
            $r = array(
              'farmacias' => array(
                'id' => $s->getId(),
                'nome' => $s->getNome(),
                'descricao' => $s->getDescricao(),
                'dataDeCriacao' => $s->getDataDeCriacao()->format('Y-m-d H:i:s'),
                'dataDeActualizacao' => $s->getDataDeActualizacao()->format('Y-m-d H:i:s')
              )
            );
            $app->response->setStatus(201);
          } else {
            $r = $erros['criacao']; #stock nao criado
          }
        } else {
          $r = $erros['criacao']; #farmacia nao encntrada
        } 
      } else {
        $r = $erros['formato'];
        $app->response->setStatus(400);

      }
    } else {
      $r = $erros['validacao'];
      $app->response->setStatus(400);
    }
    $app->response->setBody(json_encode($r));
  });

  $app->put('/:id/stockes/:sid/?', function($id) use ($app, $em, $validator, $contentType, $erros) {
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
    $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/stockes/post.json');
    $data = json_decode($app->request->getBody());
    
    $r = null;
    if ($app->request->headers->get('Content-Type') === $contentType['json'] || $app->request->headers->get('Content-Type') === $contentType['jsonapi']) {
      $validator['validator']->check($data, $schema);
      if ($validator['validator']->isValid()) {
        $farmacia = json_decode($app->request->getBody(), true);

        $nome = $farmacia['nome'];
        $descricao = $farmacia['descricao'];

        $f = $em->find('Farmacia', $id);
        $s = $em->find('Stock', $sid);

        $stockCriado = true;

        if ($f !== null) {
            if ($s !== null) {
              $em->getConnection()->beginTransaction();
              try {
                $s = new Stock;
                $s->setNome($nome);
                $s->setDescricao($descricao);
                $s->setDataDeCriacao(new DateTime('NOW'));
                $s->setDataDeActualizacao(new DateTime('NOW'));

                $f->addStocke($s);

                $em->persist($f);
                $em->flush();
              } catch (Exception $e) {
                $stockCriado = false;
              }

              if ($stockCriado) {
              $r = array(
                'farmacias' => array(
                  'id' => $f->getId(),
                  'nome' => $f->getNome(),
                  'descricao' => $f->getDescricao(),
                  'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
                  'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
                )
              );
              
              $app->response->setStatus(201);
            } else {
              $r = $erros['criacao']; # stock nao encontrado
            }
          } else {
            $r = $erros['criacao']; #farmacia nao encntrada
          } 
        } else {
          $r = $erros['validacao'];
          $app->response->setStatus(400);
        }
      } else {
        $r = $erros['formato'];
        $app->response->setStatus(400);
      }
      $app->response->setBody(json_encode($r));
    }
  });

  $app->delete('/:id/stockes/:sid/?', function($id, $sid) use ($app, $em, $validator, $contentType, $erros){
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');

    $r = null;

    $f = $em->find('Farmacia', $id);
    $s = $em->find('Stock', $sid);
    
    $stockRemovido = true;

    if ($f !== null) {
      if ($s !== null) {
        $em->getConnection()->beginTransaction();
        try {
          $em->remove($s);
          $em->flush();
          $em->getConnection()->commit();
        } catch (Exception $e) {
          $em->getConnection()->rollback();
          $stockRemovido = false;
        }

        if ($stockRemovido) {
        $r = array(
          'farmacias' => array(
            'id' => $f->getId(),
            'nome' => $f->getNome(),
            'descricao' => $f->getDescricao(),
            'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
            'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
          )
        );
        
        $app->response->setStatus(201);
        } else {
          $r = $erros['criacao']; #stock nao removido
        }
      } else {
        $r = $erros['criacao']; # stock nao encontrado
      }
    } else {
      $r = $erros['criacao']; #farmacia nao encntrada
    } 
    
    $app->response->setBody(json_encode($r));
  });
  $app->patch('/:id/stockes/:sid/?', function($id) use ($app, $em, $validator, $contentType, $erros){});

  $app->get('/:fid/stockes/:sid/categorias/?', function($fid) use ($app, $em, $validator, $contentType, $erros){})->name('categorias_do_stock');

  $app->post('/:fid/stockes/:sid/categorias/?', function($fid, $sid) use ($app, $em, $validator, $contentType, $erros){
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
    $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/stockes/post.json');
    $data = json_decode($app->request->getBody());
    
    $r = null;
    //if ($app->request->headers->get('Content-Type') === $contentType['json'] || $app->request->headers->get('Content-Type') === $contentType['jsonapi']) {
    if (true) {
      $validator['validator']->check($data, $schema);
      if ($validator['validator']->isValid()) {
        $farmacia = json_decode($app->request->getBody(), true);

        $nome = $farmacia['nome'];
        $descricao = $farmacia['descricao'];

        $f = $em->find('Farmacia', $fid);
        $s = $em->find('Stock', $sid);
        
        if ($f !== null && $s !== null) {
          if ($f->getStockes()->contains($s)) {
            $em->getConnection()->beginTransaction();

            $categoriaCriada = true;
            try {
              $c = new Categoria;
              $c->setNome($nome);
              $c->setDescricao($descricao);
              $c->setDataDeCriacao(new DateTime('NOW'));
              $c->setDataDeActualizacao(new DateTime('NOW'));

              $s->addCategoria($c);

              $em->persist($s);
              $em->flush();
              $em->getConnection()->commit();
            } catch (Exception $e) {
              $em->getConnection()->rollback();
              $categoriaCriada = true;
            }
          
            if ($categoriaCriada) {
              $r = array(
                'farmacias' => array(
                  'id' => $c->getId(),
                  'nome' => $c->getNome(),
                  'descricao' => $c->getDescricao(),
                  'dataDeCriacao' => $c->getDataDeCriacao()->format('Y-m-d H:i:s'),
                  'dataDeActualizacao' => $c->getDataDeActualizacao()->format('Y-m-d H:i:s')
                )
              );
              
              $app->response->setStatus(201);

            } else {
              echo "x";
              $r = $erros['criacao']; # categoria nao criada
            }  
        } else {
          $r =$erros['criacao'];
        }
      } else {
        $r = $erros['validacao'];
        $app->response->setStatus(400);        
      }
    } else {
      $r = $erros['validacao'];
      $app->response->setStatus(400);
    }
  } else {
    $r = $erros['formato'];
    $app->response->setStatus(400);
  }

    $app->response->setBody(json_encode($r));
  });

  $app->put('/:fid/stockes/:sid/categorias/:cid/?', function($fid, $sid, $cid) use ($app, $em, $validator, $contentType, $erros){
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
    $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/stockes/post.json');
    $data = json_decode($app->request->getBody());
    
    $r = null;
    if ($app->request->headers->get('Content-Type') === $contentType['json'] || $app->request->headers->get('Content-Type') === $contentType['jsonapi']) {
      $validator['validator']->check($data, $schema);
      if ($validator['validator']->isValid()) {
        $farmacia = json_decode($app->request->getBody(), true);

        $nome = $farmacia['nome'];
        $descricao = $farmacia['descricao'];

        $f = $em->find('Farmacia', $fid);
        $s = $em->find('Stock', $sid);
        $c = $em->find('Categoria', $cid);
        
        if ($f !== null && $s !== null && $c !== null) {
            if ($f->getStockes()->contains($s) && $s->getCategorias()->contains($c)) {
            $em->getConnection()->beginTransaction();

            $categoriaCriada = true;
            try {
              $c->setNome($nome);
              $c->setDescricao($descricao);
              //$c->setDataDeCriacao(new DateTime('NOW'));
              $c->setDataDeActualizacao(new DateTime('NOW'));


              $em->persist($c);
              $em->flush();
              $em->getConnection()->commit();
            } catch (Exception $e) {
              $categoriaCriada = false;
              $em->getConnection()->rollback();
            }
            
            if ($categoriaCriada) {
              $r = array(
                'farmacias' => array(
                  'id' => $f->getId(),
                  'nome' => $f->getNome(),
                  'descricao' => $f->getDescricao(),
                  'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
                  'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
                )
              );
              
              $app->response->setStatus(201);

            } else {
              $r = $erros['criacao']; # categoria nao criada
            }
          } else {
            $r =$erros['criacao'];
          }

        } else {

          $r = $erros['criacao']; # alguns dos recursos nao encontrados
        }
      } else {
        $r = $erros['validacao'];
        $app->response->setStatus(400);
      }
    } else {
      $r = $erros['formato'];
      $app->response->setStatus(400);
    }

    $app->response->setBody(json_encode($r));

  });
  $app->delete('/:fid/stockes/:sid/categorias/:cid/?', function($fid, $sid, $cid) use ($app, $em, $validator, $contentType, $erros){

    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
    $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/stockes/post.json');
    $data = json_decode($app->request->getBody());
    
    $r = null;
    
    $f = $em->find('Farmacia', $fid);
    $s = $em->find('Stock', $sid);
    $c = $em->find('Categoria', $cid);
    
    if ($f !== null && $s !== null && $c !== null) {
        if ($f->getStockes()->contains($s) && $s->getCategorias()->contains($c)) {
        $em->getConnection()->beginTransaction();

        $categoriaRemovida = true;
        try {
          $em->remove($c);
          $em->flush();
          $em->getConnection()->commit();
        } catch (Exception $e) {
          $categoriaRemovida = false;
          $em->getConnection()->rollback();
        }
        
        if ($categoriaRemovida) {
          $r = array(
            'farmacias' => array(
              'id' => $f->getId(),
              'nome' => $f->getNome(),
              'descricao' => $f->getDescricao(),
              'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
              'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
            )
          );
          
          $app->response->setStatus(201);

        } else {
          $r = $erros['criacao']; # categoria nao criada
        }
      } else {
        $r =$erros['criacao'];
      }

    } else {

      $r = $erros['criacao']; # alguns dos recursos nao encontrados
    }
 
    $app->response->setBody(json_encode($r));


  });
  $app->patch('/:fid/stockes/categorias/:cid/?', function($fid, $cid) use ($app, $em, $validator, $contentType, $erros){});

  $app->get('/:fid/stockes/:sid/categorias/:cid/medicamentos/?', function($fid, $sid, $cid) use ($app, $em, $validator, $contentType, $erros){})->name('medicamentos_da_categoria');
  $app->post('/:fid/stockes/:sid/categorias/:cid/medicamentos/?', function($fid, $sid, $cid) use ($app, $em, $validator, $contentType, $erros){
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
    $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/stockes/post.json');
    $data = json_decode($app->request->getBody());
    
    $r = null;
    if ($app->request->headers->get('Content-Type') === $contentType['json'] || $app->request->headers->get('Content-Type') === $contentType['jsonapi']) {
      $validator['validator']->check($data, $schema);
      if ($validator['validator']->isValid()) {
        $farmacia = json_decode($app->request->getBody(), true);

        $nome = $farmacia['nome'];
        $descricao = $farmacia['descricao'];

        $f = $em->find('Farmacia', $fid);
        $s = $em->find('Stock', $sid);
        $c = $em->find('Categoria', $cid);

        if ($f !== null && $s !== null && $c !== null) {
          if ($f->getStockes()->contains($s) && $s->getCategorias()->contains($c)) {
            $em->getConnection()->beginTransaction();

            $medicamentoCriado = true;
            try {
              $m = new Medicamento;
              $m->setNome($nome);
              $m->setDescricao($descricao);
              $m->setDataDeCriacao(new DateTime('NOW'));
              $m->setDataDeActualizacao(new DateTime('NOW'));
              $m->setPreco(0);
              $m->setQuantidade(0);

              $c->addMedicamento($m);

              $em->persist($c);
              $em->flush();

              $em->getConnection()->commit();
    
            } catch(Exception $e) {
              $em->getConnection()->rollback();
              $medicamentoCriado = false;
            }
            
            if ($medicamentoCriado) {
              $r = array(
                'farmacias' => array(
                  'id' => $m->getId(),
                  'nome' => $m->getNome(),
                  'descricao' => $m->getDescricao(),
                  'dataDeCriacao' => $m->getDataDeCriacao()->format('Y-m-d H:i:s'),
                  'dataDeActualizacao' => $m->getDataDeActualizacao()->format('Y-m-d H:i:s')
                )
              );
              $app->response->setStatus(201);
            } else {
              $r = $erros['criacao'];
            }
          } else {
            $r = $erros['criacao'];
          }          
        } else {
          $r = $erros['criacao'];          
        }
      } else {
        $r = $erros['validacao'];
        $app->response->setStatus(400);
      }
    } else {
      $r = $erros['formato'];
      $app->response->setStatus(400);
    }

    $app->response->setBody(json_encode($r));
  
  });

  $app->put('/:fd/stock/categorias/:cid/medicamentos/:mid/?', function($fid, $cid) use ($app, $em, $validator, $contentType, $erros) {
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
    $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/stockes/post.json');
    $data = json_decode($app->request->getBody());
    
    $r = null;
    if ($app->request->headers->get('Content-Type') === $contentType['json'] || $app->request->headers->get('Content-Type') === $contentType['jsonapi']) {
      $validator['validator']->check($data, $schema);
      if ($validator['validator']->isValid()) {
        $farmacia = json_decode($app->request->getBody(), true);

        $nome = $farmacia['nome'];
        $descricao = $farmacia['descricao'];

        $f = $em->find('Farmacia', $fid);
        $s = $em->find('Stock', $sid);
        $c = $em->find('Categoria', $cid);
        $m = $em->find('Medicamento', $mid);

        if ($f !== null && $s !== null && $c !== null && $mid !== null) {
          $farmaciaTemStock = $f->getStockes()->contains($s);
          $stockTemCategoria = $s->getCategorias()->contains($c);
          $categoriaTemMedicamento = $c->getMedicamentoss()->contains($m);

          if ($farmaciaTemStock && $stockTemCategoria && $categoriaTemMedicamento) {
            $em->getConnection()->beginTransaction();

            $medicamentoActualizado = true;
            try {
              $m->setNome($nome);
              $m->setDescricao($descricao);
              $m->setDataDeCriacao(new DateTime('NOW'));
              $m->setDataDeActualizacao(new DateTime('NOW'));
              $m->setPreco(0);
              $m->setQuantidade(0);

              $em->persist($m);
              $em->flush();

              $em->getConnection()->commit();
    
            } catch(Exception $e) {
              $em->getConnection()->rollback();
               $medicamentoActualizado = false;
            }
            
            if ($medicamentoActualizado) {
              $r = array(
                'farmacias' => array(
                  'id' => $f->getId(),
                  'nome' => $f->getNome(),
                  'descricao' => $f->getDescricao(),
                  'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
                  'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
                )
              );
              $app->response->setStatus(201);
            } else {
              $r = $erros['criacao'];
            }
          }          
        }
      } else {
        $r = $erros['validacao'];
        $app->response->setStatus(400);
      }
    } else {
      $r = $erros['formato'];
      $app->response->setStatus(400);
    }

    $app->response->setBody(json_encode($r));

  });
  $app->delete('/:fid/stockes/:sid/categorias/:cid/medicamentos/:mid/?', function($fid, $sid, $cid, $mid) use ($app, $em, $validator, $contentType, $erros) {
    $f = $em->find('Farmacia', $fid);
    $s = $em->find('Stock', $sid);
    $c = $em->find('Categoria', $cid);
    $m = $em->find('Medicamento', $mid);

    if ($f !== null && $s !== null && $c !== null && $mid !== null) {
      $farmaciaTemStock = $f->getStockes()->contains($s);
      $stockTemCategoria = $s->getCategorias()->contains($c);
      $categoriaTemMedicamento = $c->getMedicamentos()->contains($m);

      if ($farmaciaTemStock && $stockTemCategoria && $categoriaTemMedicamento) {
        $em->getConnection()->beginTransaction();

        $medicamentoRemovido = true;
        try {
          $em->remove($m);
          $em->flush();

          $em->getConnection()->commit();

        } catch(Exception $e) {
          $em->getConnection()->rollback();
           $medicamentoRemovido = false;
        }
        
        if ($medicamentoRemovido) {
          $r = array(
            'farmacias' => array(
              'id' => $f->getId(),
              'nome' => $f->getNome(),
              'descricao' => $f->getDescricao(),
              'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
              'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
            )
          );
          $app->response->setStatus(201);
        } else {
          $r = $erros['criacao'];
        }
      } else {
        $r = $erros['criacao'];
      }  
    } else {
      $r = $erros['criacao'];
    }       

    $app->response->setBody(json_encode($r));

  });
  $app->patch('/:fid/stock/categorias/:cid/medicamentos/:mid/?', function($fid, $cid) use ($app, $em, $validator, $contentType, $erros){});

  $app->get('/:fid/stock/categorias/:cid/medicamentos/:mid/marcadores/?', function($fid) use ($app, $em, $validator, $contentType, $erros){})->name('marcadores_do_medicamento');
  
  $app->post('/:fd/stockes/:sid/categorias/:cid/medicamentos/:mid/marcadores/?', function($fid, $sid, $cid, $mid) use ($app, $em, $validator, $contentType, $erros){
    $app->response->headers->set('Content-Type', 'application/vnd.api+json; version 1');
    $schema = $validator['retriever']->retrieve('file://' . $validator['dir'] . '/farmacias/stockes/post.json');
    $data = json_decode($app->request->getBody());
    
    $r = null;
    if ($app->request->headers->get('Content-Type') === $contentType['json'] || $app->request->headers->get('Content-Type') === $contentType['jsonapi']) {
      $validator['validator']->check($data, $schema);
      if ($validator['validator']->isValid()) {
        $json = json_decode($app->request->getBody(), true);

        $f = $em->find('Farmacia', $fid);
        $s = $em->find('Stock', $sid);
        $c = $em->find('Categoria', $cid);
        $m = $em->find('Medicamento', $mid);

        $farmaciaTemStock = $f->getStockes()->contains($s);
        $stockTemCategoria = $s->getCategorias()->contains($c);
        $categoriaTemMedicamento = $c->getMedicamentoss()->contains($m);

        $recursoNaoNulo = $f !== null && $s !== null && $c !== null && $m !== null;
        $recursoExiste = $farmaciaTemStock && $stockTemCategoria && $categoriaTemMedicamento;

        $marcadores = $json['marcadores'];
        $marcador = $json['marcador'];
        $marcadorIds = $json['ids'];
        $marcadorId = $json['id'];


        if ($recursoNaoNulo) {
          if ($recursoExiste) {
            if ($marcadorId !== null) {
              $mm = $em->find('Marcador', $marcadorId);

              $associacaoCriada = true;
              if ($mm !== null) {
                $em->getConnection()->beginTransaction();
                try {
                  $m->addMarcadore($mm);
                  $em->persist($m);
                  $em->flush();

                  $em->getConnection()->commit();                  
                } catch (Exception $e) {
                  $associacaoCriada = false;
                  $em->getConnection()->rollback();
                }
              } else {
                # nao existe marcador com id $marcadorId
              }
              
            } else if ($ids !== null) {
              foreach($marcadorIds as $i) {
                $mm = $em->find('Marcador', $i);

                $associacaoCriada = true;
                if ($mm !== null) {
                  $em->getConnection()->beginTransaction();
                  try {
                    $m->addMarcadore($mm);
                    $em->persist($m);
                    $em->flush();

                    $em->getConnection()->commit();                  
                  } catch (Exception $e) {
                    $associacaoCriada = false;
                    $em->getConnection()->rollback();
                  }
                } else {
                  # nao existe marcador com id $marcadorId
                  continue;
                }  
              }
            } else if ($marcador !== null) {
              $palavra = $marcador['palavra'];
              $descricao = $marcador['descricao'];

              $associacaoCriada = true;
              $em->getConnection()->beginTransaction();
              try {
                $marcadorNovo = new Marcador;
                $marcadorNovo->setPalavra($palavra);
                $marcadorNovo->setDescricao($descricao);
                $marcadorNovo->setDataDeCriacao(new DateTime('NOW'));
                $marcadorNovo->setDataDeActualizacao(new DateTime('NOW'));
                $m->addMarcadore($marcadorNovo);
                $em->persist($m);
                $em->flush();
                $em->getConnection()->commit();  
              } catch (Exception $e) {
                $em->getConnection()->rollback();
                $associacaoCriada = false;
              }
              
            } else {
              foreach ($marcadores as $i) {
                $palavra = $i['palavra'];
                $descricao = $i['descricao'];

                $associacaoCriada = true;
                $em->getConnection()->beginTransaction();
                try {
                  $marcadorNovo = new Marcador;
                  $marcadorNovo->setPalavra($palavra);
                  $marcadorNovo->setDescricao($descricao);
                  $marcadorNovo->setDataDeCriacao(new DateTime('NOW'));
                  $marcadorNovo->setDataDeActualizacao(new DateTime('NOW'));
                  $m->addMarcadore($marcadorNovo);
                  $em->persist($m);
                  $em->flush();
                  $em->getConnection()->commit();  
                } catch (Exception $e) {
                  $em->getConnection()->rollback();
                  $associacaoCriada = false;
                }

              }
            }
          } else {

          }
        }

        $descricao = $farmacia['descricao'];

        if ($f !== null && $s !== null && $c !== null && $m !== null) {
          $farmaciaTemStock = $f->getStockes()->contains($s);
          $stockTemCategoria = $s->getCategorias()->contains($c);
          $categoriaTemMedicamento = $c->getMedicamentoss()->contains($m);

          if ($farmaciaTemStock && $stockTemCategoria && $categoriaTemMedicamento) {
            $mr = new Marcador;
            $mr->setPalavra($nome);
            $mr->setDescricao($descricao);
            $mr->setDataDeCriacao(new DateTime('NOW'));
            $mr->setDataDeActualizacao(new DateTime('NOW'));

            $m->addMarcadore($mr);

            $em->persist($m);
            $em->flush();

            $r = array(
              'farmacias' => array(
                'id' => $f->getId(),
                'nome' => $f->getNome(),
                'descricao' => $f->getDescricao(),
                'dataDeCriacao' => $f->getDataDeCriacao()->format('Y-m-d H:i:s'),
                'dataDeActualizacao' => $f->getDataDeActualizacao()->format('Y-m-d H:i:s')
              )
            );
            $app->response->setStatus(201);
          } else {

          }
        } else {

        }
      } else {
        $r = array(
          'estado' => 'Erro',
          'codigo' => '400', 
          'descricao' => 'A requisicao enviada nao corresponde ao que necessita-se para o prosseguimento desta opercao'
        );

        $app->response->setStatus(400);

      }
    } else {
      $r = array(
        'estado' => 'Erro',
        'codigo' => '401', 
        'descricao' => 'A requisicao enviada nao corresponde ao que necessita-se para o prosseguimento desta opercao'
      );

      $app->response->setStatus(400);
    }
    $app->response->setBody(json_encode($r));
  });
  $app->put('/:fd/stock/categorias/:cid/medicamentos/:mid/marcadores/:mmid/?', function($fid, $cid) use ($app, $em, $validator, $contentType, $erros){});
  $app->delete('/:fid/stock/categorias/:cid/medicamentos/:mid/marcadores/:mmid/?', function($fid, $cid) use ($app, $em, $validator, $contentType, $erros){});
  $app->patch('/:fid/stock/categorias/:cid/medicamentos/:mid/marcadores/:mmid/?', function($fid, $cid) use ($app, $em, $validator, $contentType, $erros){});

});

$app->group('/clientes/?', function() use($app, $em, $validator, $contentType, $erros) {
  $app->get('/', function() use ($app, $em, $validator, $contentType, $erros){})->name('clientes');
  $app->post('/', function() use ($app, $em, $validator, $contentType, $erros){});
  $app->put('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->delete('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->patch('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
});

$app->group('/enderecos/?', function() use($app, $em, $validator, $contentType, $erros, $validator) {
  $app->get('/', function() use ($app, $em, $validator, $contentType, $erros){})->name('enderecos');
  $app->post('/', function() use ($app, $em, $validator, $contentType, $erros){});
  $app->put('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->delete('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->patch('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
});

$app->group('/marcadores', function() use($app, $em, $validator, $contentType, $erros) {
  $app->get('/', function() use ($app, $em, $validator, $contentType, $erros){})->name('marcadores');
  $app->post('/', function() use ($app, $em, $validator, $contentType, $erros){});
  $app->put('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->delete('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->patch('/:id/?', function($id) use ($app, $em, $validator, $contentType, $erros){});

  $app->get('/:id/medicamentos/?', function() use ($app, $em, $validator, $contentType, $erros){})->name('medicamentos_do_marcador');
  $app->post('/:id/medicamentos/?', function() use ($app, $em, $validator, $contentType, $erros){});
  $app->put('/:id/medicamentos/:mid/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->delete('/:id/medicamentos/:mid/?', function($id) use ($app, $em, $validator, $contentType, $erros){});
  $app->patch('/:id/medicamentos/:mid/?', function($id) use ($app, $em, $validator, $contentType, $erros){});

});

$app->group('/pesquisa/?', function() use($app, $em, $pdo, $validator, $contentType, $erros) {
  $app->get('/', function() use ($app, $em, $pdo, $validator, $contentType, $erros){
    
  $tipo = $app->request->params('tipo') != null ? $app->request->params('tipo') : 'farmacia';
  $palavra = $app->request->params('palavra');
  $latitude = (float)$app->request->params('latitude');
  $longitude = (float)$app->request->params('longitude');
  $limite = 10;
  $distanciaMin = $app->request->params('distancia_min') != null ? (float)$app->request->params('distancia_min') : 0;
  $distanciaMax = $app->request->params('distancia_max') != null ? (float)$app->request->params('distancia_max') : 10000;

  if ($tipo == 'medicamento') {
    if ($palavra != null && $latitude != null && $longitude != null) {
      $sql = 
      'SELECT DISTINCT
          m.id as id, 
          m.nome as nome, 
          m.preco as preco, 
          m.descricao as descricao, 
          m.quantidade as quantidade, 
          c.nome as categoria, 
          s.nome as stock, 
          f.nome as farmacia,  
          l.latitude as latitude, 
          l.longitude as longitude,
          ((SQRT((:la - l.latitude) * (:la - l.latitude) + (:lo - l.longitude) * (:lo - l.longitude))) * 111194.926645 / 1000) as distancia 
      FROM 
          marcadores ma 
          INNER JOIN marcadores_medicamentos mm ON ma.id = mm.marcador 
          INNER JOIN medicamentos m ON m.id = mm.medicamento 
          INNER JOIN categoria c ON m.categoria = c.id 
          INNER JOIN stockes s ON c.stock = s.id 
          INNER JOIN farmacias f ON f.id = s.farmacia 
          INNER JOIN localizacoes l ON l.id = f.localizacao 
      WHERE 
          (ma.palavra LIKE :p AND quantidade > 0) 
      ORDER BY 
          distancia ASC, 
          preco ASC 
      OFFSET 0  
      LIMIT :li';

      $stmt = $pdo->prepare($sql);
      $stmt->execute(
        array(
          'la' => $latitude,
          'lo' => $longitude,
          'p' => '%' . $palavra . '%', 
          'li' => $limite
        )
      );

      $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $rJson = array();

      if ($rs != null) {
        foreach ($rs as $r) {
          $rJson[] = $r;
        }

        $app->response->setStatus(200);  
        $app->response->setBody(json_encode(array($rJson)));
      } else {
        $app->response->setStatus(404);  
        $app->response->setBody(json_encode(
          array(
            'codigo' => 404,
            'descricao' => 'Nao existe medicamentos correspondentes a este criterio')));

        #nao existem medicamentos
      }

    } else {
      #parametros em falta
    }

  } else if ($tipo == 'farmacia') {
      if ($latitude != null && $longitude != null) {
     
        $sql = 
          'SELECT DISTINCT 
            f.id as id,
            f.nome as nome, 
            f.descricao as descricao, 
            l.latitude as latitude, 
            l.longitude as longitude, 
            ((SQRT((:la - l.latitude) * (:la - l.latitude) + (:lo - l.longitude) * (:lo - l.longitude))) * 111194.926645 / 1000) as distancia
          FROM 
            farmacias f INNER JOIN localizacoes l ON f.localizacao = l.id 
          ORDER BY
           distancia ASC     
          LIMIT :li';

        $stmt = $pdo->prepare($sql);
        $stmt->execute(
          array(
            'la' => $latitude,
            'lo' => $longitude,
            'li' => $limite
          )
        );

        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $rJson = array();

        if ($rs != null) {
          foreach ($rs as $r) {
            $rJson[] = $r;
          }
          $app->response->setStatus(200);
          $app->response->setBody(json_encode(array($rJson)));
        } else {
          $app->response->setStatus(404);  
          $app->response->setBody(json_encode(
            array(
              'codigo' => 404,
              'descricao' => 'Nao existe farmacias correspondentes a este criterio')));

          #nao existem farmacias
        }

      } else {
        $app->response->setStatus(400);  
        $app->response->setBody(json_encode($erros['validacao']));
        #parametros em falta
      }
    } else {
        $app->response->setStatus(400);  
        $app->response->setBody(json_encode($erros['validacao']));
      #tipo desconhecido
    }
  
  })->name('pesquisa');
});

$app->run();